﻿using System.Collections.Generic;
using GoogleARCore;
using GoogleARCore.Examples.Common;
using UnityEngine;

#if UNITY_EDITOR
// Set up touch input propagation while using Instant Preview in the editor.
using Input = GoogleARCore.InstantPreviewInput;
#endif
    
public class MainController : MonoBehaviour
{
    public Camera FirstPersonCamera;        
    public GameObject DetectedPlanePrefab;

    public GameObject SpawnPrefab;
    public GameObject selectedPlanter;

    private bool selectActionStarted = false;

    private Quaternion initialSelectedQuaternion;
    private float lastAngle = -1;
    private float rotationAcumulated = -1;

    public Animator stateMachine;
    public CommandManager commandManager;

    public LayerMask planterMask;
        
    private const float k_ModelRotation = 180.0f;
        
    private List<DetectedPlane> m_AllPlanes = new List<DetectedPlane>();
        
    private bool m_IsQuitting = false;

    public static int planterCount = 0;

    private const int maxPlanters = 5;
    private const int lostTrackingSleepTimeout = 15;

    public void Update()
    {
        _UpdateApplicationLifecycle();

        // Hide snackbar when currently tracking at least one plane.
        Session.GetTrackables<DetectedPlane>(m_AllPlanes);
        bool showSearchingUI = true;
        for (int i = 0; i < m_AllPlanes.Count; i++)
        {
            if (m_AllPlanes[i].TrackingState == TrackingState.Tracking)
            {
                showSearchingUI = false;
                break;
            }
        }

        stateMachine.SetBool("planes detected", !showSearchingUI);

        foreach(Touch screenTouch in Input.touches)
        {
            if(FirstPersonCamera.ScreenToViewportPoint(screenTouch.position).y < 0.1)
            {
                return;
            }
        }

        Touch touch = default(Touch);
        Touch secondTouch = default(Touch);
        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon | TrackableHitFlags.FeaturePointWithSurfaceNormal;

        // If a planter is selected do planter commands/actions
        if (selectedPlanter)
        {
            if (Input.touchCount > 0) { touch = Input.GetTouch(0); }
            if (Input.touchCount > 1) { secondTouch = Input.GetTouch(1); }

            if(selectActionStarted) {
                if (Input.touchCount == 1 && touch.phase == TouchPhase.Ended)
                {
                    if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit))
                    {
                        MoveCommand command = commandManager.MoveInstance(selectedPlanter, hit);
                        selectActionStarted = false;
                    }
                }

                if (Input.touchCount == 2)
                {
                    Vector2 p1 = touch.position;
                    Vector2 p2 = secondTouch.position;
                    float angle = Mathf.Atan2(p2.y - p1.y, p2.x - p1.x) * 180f / Mathf.PI;

                    if (secondTouch.phase == TouchPhase.Began)
                    {
                        initialSelectedQuaternion = selectedPlanter.transform.rotation;
                        lastAngle = angle;
                        rotationAcumulated = 0;
                    }
                    else if (touch.phase == TouchPhase.Moved || secondTouch.phase == TouchPhase.Moved)
                    {
                        float deltaAngle = angle - lastAngle;
                        selectedPlanter.transform.Rotate(0, deltaAngle, 0);
                        rotationAcumulated += deltaAngle;
                    }

                    if (touch.phase == TouchPhase.Ended || secondTouch.phase == TouchPhase.Ended)
                    {
                        selectedPlanter.transform.rotation = initialSelectedQuaternion;
                        RotateCommand command = commandManager.RotateInstance(selectedPlanter, rotationAcumulated);
                        selectActionStarted = false;
                    }
                    lastAngle = angle;
                }
            }

            if (touch.phase == TouchPhase.Began)
            {
                selectActionStarted = true;
            }

            return;
        }
        //else if (stateMachine.GetInteger("step") == 4 && selectedPlanter == null)
        //{
        //    stateMachine.SetTrigger("prevStep");
        //    selectActionStarted = false;
        //}

        // If the player has not touched the screen, we are done with this update.
        if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
        {
            return;
        }

        // Raycas with planters to try to select one
        Ray cameraRay = FirstPersonCamera.ScreenPointToRay(touch.position);
        RaycastHit rayHit;
        if(Physics.Raycast(cameraRay, out rayHit, 1000, planterMask))
        {
            SelectPlanter(rayHit.collider.gameObject);
            return;
        }

        // Raycast against the location the player touched to search for planes.
        if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit))
        {
            // Use hit pose and camera pose to check if hittest is from the
            // back of the plane, if it is, no need to create the anchor.
            if ((hit.Trackable is DetectedPlane) && Vector3.Dot(FirstPersonCamera.transform.position - hit.Pose.position, hit.Pose.rotation * Vector3.up) < 0)
            {
                Debug.Log("Hit at back of the current DetectedPlane");
            }
            else
            {
                int currentStep = stateMachine.GetInteger("step");
                if (currentStep == 3 || currentStep == 5)
                {
                    if (hit.Trackable is DetectedPlane && CanSpawnPlanter())
                    {
                        CreateCommand command = commandManager.CreateInstance(SpawnPrefab, hit);

                        // Compensate for the hitPose rotation facing away from the raycast (i.e. camera).
                        command.instance.transform.Rotate(0, k_ModelRotation, 0, Space.Self);

                        if(planterCount == 1)
                        {
                            SelectPlanter(command.instance);
                        }
                    }
                }
            }
        }
    }

    private bool CanSpawnPlanter()
    {
        return planterCount < maxPlanters;
    }

    public void SelectPlanter(GameObject gameObject)
    {
        selectedPlanter = gameObject;
        selectActionStarted = false;
        stateMachine.SetTrigger("nextStep");
    }

    public void UnselectPlanter()
    {
        selectedPlanter = null;
    }

    public void ChangeModel(GameObject prefab)
    {
        SpawnPrefab = prefab;
    }
        
    private void _UpdateApplicationLifecycle()
    {
        // Only allow the screen to sleep when not tracking.
        if (Session.Status != SessionStatus.Tracking)
        {
            Screen.sleepTimeout = lostTrackingSleepTimeout;
        }
        else
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        if (m_IsQuitting)
        {
            return;
        }

        // Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
        if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
        {
            AndroidMessages.ShowAndroidToastMessage("Camera permission is needed to run this application.");
            m_IsQuitting = true;
            Invoke("_DoQuit", 0.5f);
        }
        else if (Session.Status.IsError())
        {
            AndroidMessages.ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
            m_IsQuitting = true;
            Invoke("_DoQuit", 0.5f);
        }
    }
        
    private void _DoQuit()
    {
        Application.Quit();
    }
}
