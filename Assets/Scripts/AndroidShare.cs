﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidShare : MonoBehaviour {

    public string shareSubject;
    public string shareMessage;

    public ScreenRecorder screenRecorder;

    public void ShareLastImage()
    {
        ShareImage(screenRecorder.GetLastScreenshotFilename());
    }

    private void ShareImage(string imageFileName)
    {
#if UNITY_ANDROID
        new NativeShare().AddFile(imageFileName).SetSubject(shareSubject).SetText(shareMessage).Share();
#endif
    }

}
