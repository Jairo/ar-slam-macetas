﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class ScreenRecorder : MonoBehaviour
{
    // Folder to write screenshot
    public string folder;

    // To place the screen taken
    public Image nextStepBackground;

    // Trigger screenshot
    [HideInInspector]
    public bool captureScreenshot = false;

    // Events
    public UnityEvent onPreScreenshot, onPostScreenshot;

    // Components
    Camera _camera;

    private Texture2D tex;
    private RenderTexture oldRT;

#if UNITY_ANDROID
    AndroidJavaObject currentActivity;
    AndroidJavaObject context;
    AndroidJavaClass UnityPlayer;
#endif

    private void Awake()
    {
        if(!UniAndroidPermission.IsPermitted(AndroidPermission.WRITE_EXTERNAL_STORAGE))
        {
            UniAndroidPermission.RequestPermission(AndroidPermission.WRITE_EXTERNAL_STORAGE, OnAllow, OnDeny, OnDenyAndNeverAskAgain);
        }
    }

    private void OnAllow()
    {
        // execute action that uses permitted function.
    }

    private void OnDeny()
    {
        // back screen / show warnking window
    }

    private void OnDenyAndNeverAskAgain()
    {
        // show warning window and open app permission setting page
    }

    // create a unique filename using a one-up variable
    private string GetUniqueFilename()
    {
        int counter = Directory.GetFiles(folder, "screen*.png", SearchOption.TopDirectoryOnly).Length;
        return string.Format("{0}/screen{1}.png", folder, counter);
    }

    public string GetLastScreenshotFilename()
    {
        int counter = Directory.GetFiles(folder, "screen*.png", SearchOption.TopDirectoryOnly).Length - 1;
        return string.Format("{0}/screen{1}.png", folder, counter);
    }

    private Texture2D DumpRenderTexture(RenderTexture rt)
    {
        tex = new Texture2D(rt.width, rt.height);
        tex.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
        tex.Apply();

        return tex;
    }

    private void Update()
    {
        if (captureScreenshot)
        {
            captureScreenshot = false;
            StartCoroutine(TakeScreenshot());
        }
    }

    IEnumerator TakeScreenshot()
    {
        yield return new WaitForEndOfFrame();

        onPreScreenshot.Invoke();

        // create render texture
        RenderTexture rt;
        rt = new RenderTexture(Screen.width, Screen.height, 24, RenderTextureFormat.ARGB32);
        rt.useMipMap = false;
        rt.antiAliasing = 1;

        // render
        oldRT = RenderTexture.active;
        RenderTexture.active = rt;
        _camera.targetTexture = rt;
        _camera.Render();

        nextStepBackground.sprite = Sprite.Create(DumpRenderTexture(rt),
                                                  new Rect(0, 0, Screen.width, Screen.height),
                                                  new Vector2(.5f, .5f));

        onPostScreenshot.Invoke();
    }

    public void SaveScreenshot()
    {
        if (!Directory.Exists(folder))
        {
            Directory.CreateDirectory(folder);
        }

        File.WriteAllBytes(GetUniqueFilename(), tex.EncodeToPNG());
        AndroidMessages.ShowAndroidToastMessage("Tu foto fue guardada en la raiz de tu dispositivo");
    }

    public void CleanRenderTexture()
    {
        _camera.targetTexture = null;
        RenderTexture.active = oldRT;
    }

    private void Start()
    {
        _camera = GetComponent<Camera>();
    }

    public void CaptureScreenshot()
    {
        captureScreenshot = true;
    }
}