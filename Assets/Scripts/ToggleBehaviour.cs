﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleBehaviour : MonoBehaviour
{
    [SerializeField]
    bool _isOn;

    public bool IsOn { set { _isOn = value; gameObject.SetActive(value); } get { return _isOn; } }
	
	// Update is called once per frame
	void Awake ()
    {
        IsOn = _isOn;
    }

    public void Toggle ()
    {
        IsOn = !_isOn;
    }
}
