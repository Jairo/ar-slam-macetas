﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class DraggableAndClickableBehaviour : MonoBehaviour, IPointerClickHandler
{
    public UnityEvent onClick;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (onClick != null)
            onClick.Invoke();
    }
}
