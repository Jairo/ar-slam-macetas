﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;

public abstract class AbstractCommand
{
    public abstract void Run();
    public abstract void Undo();
}

public class CreateCommand : AbstractCommand
{
    private GameObject prefab;
    private TrackableHit hit;

    public GameObject instance;
    public Anchor anchor;

    public CreateCommand(GameObject prefab, TrackableHit hit)
    {
        this.prefab = prefab;
        this.hit = hit;
    }

    override public void Run() {
        instance = GameObject.Instantiate(prefab, hit.Pose.position, hit.Pose.rotation);

        anchor = hit.Trackable.CreateAnchor(hit.Pose);
        instance.transform.parent = anchor.transform;

        MainController.planterCount += 1;
    }

    override public void Undo()
    {
        GameObject.Destroy(anchor.gameObject);

        MainController.planterCount -= 1;
    }
}

public class MoveCommand : AbstractCommand
{
    private GameObject instance;
    private TrackableHit hit;

    private Vector3 oldPosition;

    public MoveCommand(GameObject instance, TrackableHit hit)
    {
        this.instance = instance;
        this.hit = hit;

        oldPosition = instance.transform.position;
    }

    override public void Run()
    {
        instance.transform.position = hit.Pose.position;
    }

    override public void Undo()
    {
        instance.transform.position = oldPosition;
    }
}

public class RotateCommand : AbstractCommand
{
    private GameObject instance;
    private float rotationY;

    private Quaternion oldRotation;

    public RotateCommand(GameObject instance, float rotationY)
    {
        this.instance = instance;
        this.rotationY = rotationY;

        oldRotation = instance.transform.rotation;
    }

    override public void Run()
    {
        instance.transform.Rotate(0, rotationY, 0);
    }

    override public void Undo()
    {
        instance.transform.rotation = oldRotation;
    }
}

public class CommandManager : MonoBehaviour {

    public Animator stateMachine;
    public Stack<AbstractCommand> commandHistory;

	private void Start () {
        commandHistory = new Stack<AbstractCommand>();
    }

    public CreateCommand CreateInstance(GameObject prefab, TrackableHit hit)
    {
        CreateCommand command = new CreateCommand(prefab, hit);
        command.Run();
        commandHistory.Push(command);
        return command;
    }

    public MoveCommand MoveInstance(GameObject instance, TrackableHit hit)
    {
        MoveCommand command = new MoveCommand(instance, hit);
        command.Run();
        commandHistory.Push(command);
        return command;
    }

    public RotateCommand RotateInstance(GameObject instance, float rotationY)
    {
        RotateCommand command = new RotateCommand(instance, rotationY);
        command.Run();
        commandHistory.Push(command);
        return command;
    }

    public void UndoCommand()
    {
        if (commandHistory.Count > 0)
        {
            commandHistory.Pop().Undo();

            // si eso fue lo ultimo para deshacer (por ende, no hay nada en la escena), salir del modo edicion
            if (commandHistory.Count == 0 && stateMachine.GetInteger("step") == 4)
            {
                stateMachine.SetTrigger("back to select");
            }
        }
        else
        {
            AndroidMessages.ShowAndroidToastMessage("No hay mas acciones para revertir");
        }
    }

    public void ClearCommand()
    {
        while (commandHistory.Count > 0) commandHistory.Pop().Undo();
    }

    private void LateUpdate()
    {
        // Back button on Android
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            int step = stateMachine.GetInteger("step");
            switch (step)
            {
                case 0:
                case 2:
                    Application.Quit();
                    break;

                case 3:
                    if (commandHistory.Count > 0)
                    {
                        commandHistory.Pop().Undo();
                    }
                    else
                    {
                        stateMachine.SetTrigger("prevStep");
                    }
                    break;

                case 4:
                    if (commandHistory.Count > 0)
                    {
                        commandHistory.Pop().Undo();

                        if (commandHistory.Count == 0)
                        {
                            stateMachine.SetTrigger("back to select");
                        }
                    }
                    break;

                default:
                    stateMachine.SetTrigger("prevStep");
                    break;
            }
        }
    }

}
