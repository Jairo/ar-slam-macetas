﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class StateMachine : MonoBehaviour
{
    public GameObject[] steps;

    Animator _animator;

    // Use this for initialization
    void Awake()
    {
        _animator = GetComponent<Animator>();
        foreach (GameObject step in steps) step.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
